import {RoomModel} from './room.model';

export interface HostelModel {
    id?: number;
    name: string;
    roomNumbers: number;
    pool: boolean;
    rooms?: RoomModel[] | any;

}
