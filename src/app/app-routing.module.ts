import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'hostels', loadChildren: () => import('./home/home.module').then(m => m.HomeModule) },
  { path: 'hostels/:id', loadChildren: () => import('./detail/detail.module').then(m => m.DetailModule) },
  { path: 'hostels/:id/edit', loadChildren: () => import('./edit/edit.module').then(m => m.EditModule) },
  { path: '', redirectTo: '/hostels', pathMatch: 'full' }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
