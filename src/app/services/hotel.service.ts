import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HostelModel} from '../models/hostel.model';

@Injectable({
  providedIn: 'root'
})
export class HotelService {
root: string = environment.api;
  constructor(
    private httpClient: HttpClient
  ) { }
  getAllHotel$(): Observable<HostelModel[]>{
return this.httpClient.get(this.root + 'hostels') as Observable<HostelModel[]>;
  }

  getAllHotelById$(hotelId: string): Observable<HostelModel>{
return this.httpClient.get(this.root + 'hostels/' + hotelId ) as Observable<HostelModel>;
  }
  deleteHotelById$(hotelId: string): Observable<HostelModel> {
return this.httpClient.delete(this.root + 'hostels/' + hotelId ) as Observable<HostelModel>;
  }
  updateHotelById$(hotelId: string): Observable<HostelModel> {
return this.httpClient.patch(this.root + 'hostels/' , hotelId) as Observable<HostelModel>;
  }
}
