import {Component, OnDestroy, OnInit} from '@angular/core';
import {HotelService} from '../services/hotel.service';
import {HostelModel} from '../models/hostel.model';
import {takeUntil, tap} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
hostels: HostelModel[];
destroy$: Subject<boolean> = new Subject();
  constructor(
    private hotelService: HotelService
  ) { }

  ngOnInit(): void {

    this.hotelService.getAllHotel$()
      .pipe(
        tap(x => this.hostels = x),
      )
      .pipe
      (takeUntil(this.destroy$)
      ).subscribe();


  }
ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
}
}
