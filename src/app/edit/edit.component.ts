import {Component, OnDestroy, OnInit} from '@angular/core';
import {HostelModel} from '../models/hostel.model';
import {Subject} from 'rxjs';
import {HotelService} from '../services/hotel.service';
import {takeUntil, tap} from 'rxjs/operators';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit, OnDestroy {

  constructor(
  ) { }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {

  }
}
