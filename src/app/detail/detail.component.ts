import {Component, OnDestroy, OnInit} from '@angular/core';
import {HostelModel} from '../models/hostel.model';
import {Subject} from 'rxjs';
import {HotelService} from '../services/hotel.service';
import {takeUntil, tap} from 'rxjs/operators';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy {
  hostels: HostelModel;
  destroy$: Subject<boolean> = new Subject();
  hotelId: string;
  route: any;
  constructor( private hotelService: HotelService
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {this.hotelId = params.get('id'); });
    this.hotelService.getAllHotelById$('')
      .pipe(
        tap(x => this.hostels = x),
      )
      .pipe
      (takeUntil(this.destroy$)
      ).subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
